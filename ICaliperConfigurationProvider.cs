﻿using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Caliper.Interfaces
{
	public interface ICaliperConfigurationProvider
	{
		Task<ICaliperConfiguration> GetCaliperConfigurationAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}