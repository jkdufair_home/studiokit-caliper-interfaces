﻿namespace StudioKit.Caliper.Interfaces
{
	public interface ICaliperConfiguration
	{
		bool CaliperEnabled { get; }

		string CaliperEventStoreClientId { get; }

		string CaliperEventStoreClientSecret { get; }

		string CaliperEventStoreHostname { get; }

		string CaliperPersonNamespace { get; }
	}
}